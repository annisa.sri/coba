# Repository Instruksi Lab

CSGE602022 - Perancangan & Pemrograman Web @
Fakultas Ilmu Komputer - Universitas Indonesia, Semester Genap 2017/2018

---

## Table of Contents

1. [Lab 1](lab_instruction/lab_1/README.md) - Introduction to Git (on GitLab) & TDD (Test-Driven Development) with Django
2. [Lab 2](lab_instruction/lab_2/README.md) - Introduction to Django Framework
3. [Lab 3](lab_instruction/lab_3/README.md) - Introduction to _Models_ Django and Heroku Database with TDD Discipline
4. [Lab 4](lab_instruction/lab_4/README.md) - Pengenalan _HTML5_
5. [Lab 5](lab_instruction/lab_5/README.md) - Pengenalan _CSS_
6. [Lab 6](lab_instruction/lab_6/README.md) - Pengenalan _Javascript dan JQuery_
7. [Lab 7](lab_instruction/lab_7/README.md) - Pengenalan _Web Service_
8. [Lab 8](lab_instruction/lab_8/README.md) - Pengenalan Pengenalan _Oauth2_
9. [Lab 9](lab_instruction/lab_9/README.md) - Oauth, _Webservice_, beserta Pengenalan _Cookie_ dan _Session_
10. [Lab 10](lab_instruction/lab_10/README.md) - Penerapan Lanjut _Cookie_ dan _Session_

